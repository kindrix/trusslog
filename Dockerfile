FROM swipl
COPY . /app

RUN apt-get update -y \
    	&& apt-get install -y curl gnupg \
	&& curl -sL https://deb.nodesource.com/setup_11.x | bash \
	&& apt-get install -y nodejs

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "start"]
