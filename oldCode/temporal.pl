
/*
Load program
*/
:- [flightLog, missionSpecs].

/*
Helpers
//missionSpec to log format
//Eg. value(altitude, 50) -> altitude(50)
*/



/*
Initialization
- load missionSpecs and logs
*/

start:- missionSpecs(ListSpecs),
        getAllLogs(AllLogs),
        start(ListSpecs, AllLogs).

start([], _).

start([H|T], AllLogs):-
              call(H, AllLogs),
              start(T, AllLogs).

/*
Get all logs
*/

getAllLogs(LogList) :-
              writeln("---------------"),
              writeln("----- Logs ----"),
              writeln("---------------"),
              bagof(log(X,Y), log(X, Y), LogList),
              writeln(LogList),
              writeln(""),
              writeln("---------------"),
              writeln("- Being Check -"),
              writeln("---------------").


/*
Check whether a property or a list of property holds in a log entry
*/
holds(X, log(_, Data)) :- member(X, Data).

/*
Check differnt modes
*/



% check property X has value Y in Log
check(value(X,Y), Log) :- holds(value(X,Y), Log).

% check property X has value Y in Log (used for location)
check(value(X,Y,Z), Log) :- holds(value(X,Y,Z), Log).

% check property X has value between Y and Z inclusive in Log
check(valueRange(X,Y,Z), Log) :- holds(value(X,A), Log), Y =< A, A =< Z.

% check location property with coords and radius in Log
% check(valueRange(X,Y,Z,R), Log) :- holds(value(X,A), Log), Y =< A, A =< Z.

% check whether list of properties hold in a log
check([H|T], Log) :- check(H, Log),
                        check(T, Log).
check([], _).


% checks whether two list of properties holds together
check(holdsTogether(L1, L2), Log) :- check(L1, Log),
                                        check(L2, Log).

% holds in the next log
check(holdsNext(L1, L2), Log, NextLog) :-
                    (
                      check(L1, Log) -> check(L2, NextLog);
                      true
                    ).



% holds sometime in future
check(holdsSometime(L1), Log, NextLog) :- check(L1, Log),
                                       check(L2, NextLog).

/*
Check X holds in each log
- X can be a property or a list of properties
*/

holdsAlways(holdsNext(X, Y), [Log, NextLog | T], Result) :-
                       (
                        check(holdsNext(X,Y), Log, NextLog) ->
                          !,
                          holdsAlways(holdsNext(X, Y), [NextLog | T], R2),
                          Result = R2;
                          Result = false
                       ).

holdsAlways(holdsNext(_, _), [_| []], true).


holdsAlways(X, [Log|T], Result) :-
                       (
                        check(X, Log) ->
                          holdsAlways(X, T, R2),
                          Result = R2;
                          Result = false
                       ).

holdsAlways(_, [], true).

/*
Always : Check Safety Conditions
*/

always([], _):- !.
always([H|T], AllLogs) :- write('Checking: Always() -> '),
                 write(H),
                 holdsAlways(H, AllLogs, Result),
                 write(" -> "), writeln(Result),
                 always(T, AllLogs).

always(_):- print(' -> FAIL').

/*
Eventually
*/

holdsEventually(X, [Log|T], Result) :-
                       (
                        check(X, Log) ->
                          Result = true;
                          holdsEventually(X, T, R2),
                          Result = R2
                       ).

holdsEventually(_, [], false).


eventually([], _).

eventually([H|T], AllLogs) :- write('Checking: Eventually() -> '),
                 write(H),
                 holdsEventually(H, AllLogs, Result),
                 write(" -> "), writeln(Result),
                 eventually(T, AllLogs).
