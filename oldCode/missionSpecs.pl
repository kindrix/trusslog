
missionSpecs([
  %always
  always([
    value(altitude, 50),
    value(fuel, 45),
    valueRange(altitude, 30, 60),
    holdsTogether(
      [value(fuel, 45)],
      [valueRange(altitude, 30, 60)]
    ),
    holdsNext(
      [value(location, 9, 4), valueRange(altitude, 10,100)],
      [value(camera, on)]
    )
  ]),
  %future
  eventually([
    value(altitude, 20),
    value(camera, off)
  ])

]).

/*
holdsNext([],[]),
holdsSometime([],[])
*/
