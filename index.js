//Load express module with `require` directive
var express = require('express')
var app = express()

var shelljs = require('shelljs')


app.set('view engine', 'pug')
app.set('views', './views')
app.use('/codemirrorJS', express.static(__dirname + '/node_modules/codemirror/lib/'));
app.use('/codemirrorCSS', express.static(__dirname + '/node_modules/codemirror/lib/'));
app.use('/codemirrorTheme', express.static(__dirname + '/node_modules/codemirror/theme/'));
app.use(express.static('public'))


app.use(express.json());


//Define request response in root URL (/)
app.get('/', function (req, res) {
  res.render('index')
})

app.get('/documentation', function (req, res) {
  res.render('documentation')
})

app.get('/quickstart', function (req, res) {
  res.render('quickstart')
})

//Launch listening server on port 8081
app.listen(8080, function () {
  console.log('app listening on port 8081!')
})

// process request
app.post("/analyze", function(req, res){
  //console.log(req.body)
  var f = req.body.log + req.body.mission
  shelljs.echo(f).to('analyzer/temp/input.pl')
  shelljs.exec('swipl -f analyzer/main.pl -g start -g halt', function(code, stdout, stderr){
    console.log("output:" + stdout)
    console.log("exting prolog:")
    res.status(200).send({data:stdout})
  })
})
