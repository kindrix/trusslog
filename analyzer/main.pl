/*
* Main program for Analyzer
*/


/*
* Load files
*/

% offline version; comment if using online version
%:- [flightLog, missionReqs, tempOps].

% onlive version; uncomment below
:- [temp/input, tempOps].

/*
* Initialization
*/

start :- printStep("Reading Logs"),
         readLogsIntoList(LogList), % save logs into a list, useful for some ops,
         printStep("Reading Timepoints"),
         getAllTimePoints,
         printStep("Reading Mission Requirements"),
         readReqsIntoList(ReqList),
         % missionReqs(ReqList),
         start(ReqList, LogList).

 /*
 * Evaluate Requirements
 */

start([], _).

start([req(Name, Priority, F) | Rem ], LogList) :-
          writeln(""),
          writeln("\n********** Requirement **************"),
          writeln(""),
          string_concat("Name: ", Name, S),
          writeln(S),
          writeln(""),
          %trace,
          eval(F, Result, ""),
          writeln(""),
          write("OUTPUT"),
          string_concat("{\"name\":\"", Name, S1),
          string_concat(S1, "\", ", S2),
          string_concat(S2, "\"priority\":\"", S3),
          string_concat(S3, Priority, S4),
          string_concat(S4, "\", ", S5),
          string_concat(S5, "\"result\":\"", S6),
          string_concat(S6, Result, S7),
          string_concat(S7, "\"}", S8),
          writeln(""),
          writeln(S8),
          writeln(""),
          writeln("********** End Requirement **************"),
          writeln(""),
          start(Rem, LogList).




% Conjunction
eval([], 1, _):- !.
eval([H|T], Result, Spacer) :- !,
                             printChecking([H|T], "?", Spacer),
                             string_concat(Spacer, "   ", NewSpacer),
                             (
                              eval(H, R1, NewSpacer),
                              R1 = 1 ->
                                (
                                 eval(T, R2, NewSpacer),
                                 Result is R1 * R2
                                );
                                Result = 0
                             ),
                             printChecking([H|T], Result, Spacer).


% Disjunction
eval(or(F1, F2), Result, Spacer) :- !,
                                    printChecking(or(F1, F2), "?", Spacer),
                                    string_concat(Spacer, "   ", NewSpacer),
                                    eval(F1, R1, NewSpacer),
                                    (R1 = 1 ->
                                      (Result = R1);
                                      (eval(F2, R2, NewSpacer), Result = R2)
                                    ),
                                    printChecking(or(F1, F2), Result, Spacer).

% Conditional
eval(if(F1, F2), Result, Spacer) :- !,
                                    printChecking(if(F1,F2), "?", Spacer),
                                    string_concat(Spacer, "   ", NewSpacer),
                                    eval(F1, R1, Spacer),
                                    (R1 = 0 ->
                                      (Result = R1);
                                      (eval(F2, R2, NewSpacer), Result = R2)
                                    ),
                                    printChecking(if(F1,F2), Result, Spacer).

% holds
eval(holds(Time, F), Result, Spacer) :- !,
                                        holds(Time, F, Result, Spacer).


% holdsBetween
eval(holdsBetween(Time1, Time2, F), Result, Spacer) :-!,
                                      printChecking("holdsBetween(_)", "?", Spacer),
                                      string_concat(Spacer, "   ", NewSpacer),
                                      holdsBetween(Time1, Time2, F , LogList, Result),
                                      printChecking("holdsBetween(_)", Result, Spacer).


% next
eval(next(F1, F2), Result, Spacer) :- %get all time poin ts
                                      allTimePoints(TLIN),
                                      %trace,
                                      %returns all time points in TLIN where next is true and stores them in TLOUT
                                      next(F1, F2, Result, TLIN, TLOUT, Spacer).


% always
eval(always(F1, F2), Result, Spacer) :- %get all time points
                                      allTimePoints(TLIN),
                                      %trace,
                                      %returns all time points in TLIN where next is true and stores them in TLOUT
                                      always(F1, F2, Result, TLIN, TLOUT, Spacer).


% eventually
eval(eventually(F1, F2), Result, Spacer) :- %get all time points
                                      allTimePoints(TLIN),
                                      %trace,
                                      %returns all time points in TLIN where next is true and stores them in TLOUT
                                      eventually(F1, F2, Result, TLIN, TLOUT, Spacer).


/*******************************
*********** Helpers  ***********
********************************/


% Get all logs and store them in a list
% also find last time point which is useful for some operations
readLogsIntoList(Logs) :- bagof(log(X,Y), log(X, Y), Logs),
                          last(Logs, log(T, _)),
                          assert(logList(Logs)),
                          assert(endTime(T)).


% Get all requirements and store them in a list
readReqsIntoList(ReqList) :- bagof(req(X,Y,Z), req(X,Y,Z), ReqList).


% get all time points and store them in a predicate 'allTimePoints'
getAllTimePoints :- findall(Time, log(Time, _), TimeList),
                    % all time points
                    assert(allTimePoints(TimeList)),

                    % assert last time point
                    last(TimeList, Last),
                    assert(lastTimePoint(Last)).


% increment list of timepoints by 1
addOneToTimePoints([], []) :- !.
addOneToTimePoints([StartTime|T], NewTimeList) :- T1 is StartTime + 1,
                                           addOneToTimePoints(T, Z),
                                           NewTimeList = [T1 | Z].

% Formatting
printStep(S) :- nl,
                write(S),
                write(" "),
                writeln("\u2713").

% Formatting
printHeader(S) :- nl,
                  writeln("--------------------------------"),
                  write("---------"),
                  write(S),
                  writeln(" ---------"),
                  writeln("--------------------------------").

% Formatting
printChecking(X, Result, Spacer) :-
                         write(Spacer),
                         write(X),
                         write(" -> "),
                         writeln(Result).
