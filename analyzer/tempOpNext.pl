/*next(p, q)
next(next(p,q), r)
next(next(p,q), next(r, s))


next(F1, F2) :- next(F1, F2, T1)
next(F1, F2, T1)

next(F1, F2) :- next(F1, F2, TF1, TF2).

next(val(alt,50),val(fuel,40),R).
next(val(alt,50),or(val(fuel,40),val(loc, 6, 4)),R).
next(or(val(fuel,40),val(loc, 6, 4)),val(alt,50),R).

next(val(alt,50),val(alt,40),R).
next(next(val(alt,50),val(alt,40)), val(fuel,45)).
next(val(fuel, 45), next(val(alt,50),val(alt,40)), R ).
*/


next(F1, F2, Result, Spacer) :- %get all time points
                allTimePoints(TL),
                %find all where F1 i true, store in TL1
                findAllWhereTrue(F1, TL, TL1),
                %find all next timepoints of each in TF1 store in TF2
                addOneToTimePoints(TL1, TL2),
                %check F2 holds in each TF2
                checkHolds(F2, TL2, Result).




findAllWhereTrue(next(F1, F2), [], _,  1) :- !.
findAllWhereTrue(next(F1, F2), TL, TLR) :- findAllWhereTrue(F1, TL, TL1),
                                    addOneToTimePoints(TL1, TL2),
                                    checkHolds(F2, TL2, R),
                                    (R = 1 ->
                                    TLR = TL1;
                                    TLR = []
                                    ).

findAllWhereTrue(F, [], []) :- !.
findAllWhereTrue(F, [H|TL], TL1) :- !, findAllWhereTrue(F, TL, TL2),
                                            holdsAt(H, F, R1, ""),
                                            (R1 = 1 ->
                                             TL1 = [H | TL2];
                                             TL1 = TL2
                                           ).

checkHolds(next(F1, F2), [], 1) :- !.
checkHolds(next(F1, F2), TL, Result) :- findAllWhereTrue(F1, TL, TL1),
                                        addOneToTimePoints(R1, TL2),
                                        checkHolds(F2, TL2, Result).

checkHolds(F, [], 1) :- !.
checkHolds(F, [H|TL], Result) :- holdsAt(H, F, R1, ""),
                                        ( R1 = 1 ->
                                          checkHolds(F, TL, R2),
                                          Result is R1 * R2;
                                          Result = 0
                                        ).
