/**************************************
** holds(Time, Formula, Result) *****
***************************************/

% true holds in every log entry
holds(_, true, 1, _) :- !.
                        %printChecking(holds(T, true), 1, Spacer).

% property P has value X at T
holds(T, val(P, X), Result, Spacer) :- !,
                          (
                            log(T, Vals),
                            member(val(P, X), Vals) ->
                            printChecking(holds(T, val(P,X)), 1, Spacer),
                            Result = 1;
                            printChecking(holds(T, val(P,X)), 0, Spacer),
                            Result = 0
                           ).

 % property P has value between X, Y at T
 holds(T, range(P, X, Y), Result, Spacer) :-!,
                            (
                             log(T, Vals),
                             member(val(P, Z), Vals),
                             Z =< Y, Z >= X ->
                             printChecking(holds(T,range(P, X, Y)), 1, Spacer),
                             Result = 1;
                             printChecking(holds(T,range(P, X, Y)), 0, Spacer),
                             Result = 0
                            ).


% property P has value X, Y at T
% used for location
holds(T, val(P, X, Y), Result, Spacer) :- !,
                            (
                              log(T, Vals),
                              member(val(P, X, Y), Vals) ->
                                printChecking(holds(T,val(P, X, Y)), 1, Spacer),
                                Result = 1;
                                printChecking(holds(T, val(P, X, Y)), 0, Spacer),
                                Result = 0
                            ).

%
% property P has value within radius R of X, Y at T
% used for location
% location is with R (radius units) from X and Y

holds(T, range(P, X, Y, R), Result, Spacer) :-
                              log(T, Vals),
                              member(val(P, X1, Y1), Vals),
                              Radius is sqrt((X-X1)**2 + (Y-Y1)**2),
                              Radius =< R ->
                                  printChecking(range(P, X, Y, R), 1, Spacer),
                                  Result = 1;
                                  printChecking(range(P, X, Y, R), 0, Spacer),
                                  Result = 0.

/*
* Compound Formula: OR/2
* Syntax: or(Formula, Formula)
*/

holds(Time, or(F1, F2), Result, Spacer):-
                                printChecking(or(Time, F1, F2), "?", Spacer),
                                string_concat(Spacer, "   ", NewSpacer),
                                holds(Time, F1, R1, NewSpacer),
                                (R1 = 1 ->
                                  Result = R1;
                                  (
                                    holds(Time, F2, R2, NewSpacer),
                                    Result = R2
                                  )
                                ),
                                printChecking(or(Time, F1, F2), Result, Spacer).


/*
* Compound Formula: Implication
* Syntax: [Formulas]
*/

holds(Time, if(F1, F2), Result, Spacer):-
                                printChecking(if(F1, F2), "?", Spacer),
                                string_concat(Spacer, "   ", NewSpacer),
                                (
                                  holds(Time, F1, R1, NewSpacer),
                                  R1 = 0 ->
                                  Result = R1;
                                  holds(Time, F2, R2, NewSpacer),
                                  Result = R2
                                ),
                                printChecking(if(F1, F2), Result, Spacer).


/*
* Compound Formula: Conjunction
* Syntax: [Formulas]
*/

holds(_, [], 1, _).
holds(T, [Formula | Rem], Result, Spacer) :-
                                  printChecking(holds(T, [Formula | Rem]), "?", Spacer),
                                  string_concat(Spacer, "   ", NewSpacer),
                                  holds(T, Formula, R1, NewSpacer),
                                  (
                                    R1 = 1 ->
                                    holds(T, Rem, R2, NewSpacer),
                                    Result is R1 * R2;
                                    Result = 0
                                  ),
                                  printChecking(holds(T, [Formula | Rem]), Result, Spacer).


/***********************
***** holds between *****
************************/

%
% list of properties that hold at all times between T1 and T2
%

% helper - navigate to correct timepoint in log
navigateToCorrectTimePoint(Time1, Time2,  [log(Time, Vals) | T], NewLogList) :-
                      Time =< Time2,
                      (
                        Time1 =< Time ->
                        NewLogList = [log(Time, Vals) | T];
                        navigateToCorrectTimePoint(Time1, Time2, T, NewLogList)
                      ).


% helper to make sure Time1, Time2 and Time3 satisfy some constraints
checkHoldsAtEachBetweenTimeCons(Time1, Time2, Time) :-
                      % Time1 must be less than equal to Time2
                      Time1 =< Time2,
                      % Time must be between Time1 and Time2
                      Time1 =< Time,
                      Time =< Time2.

% preprocessor for holds between; returns list adjusted to start Time1
holds(Time1, Time2, Vals , LogList, Result) :-
                            navigateToCorrectTimePoint(Time1, Time2, LogList, NewLogList ) ->
                            %trace,
                            holdsBefore(Time2, Vals , NewLogList, Result);
                            Result = 0.

holdsBefore(EndTime, Vals , [log(Time, _) | []], Result) :-
                            !,
                            (
                              Time > EndTime ->
                              Result = 1;
                              holds(Time, Vals, Result)
                            ).


holdsBefore(EndTime, Vals , [log(Time, _) | T], Result) :-
                            (
                              Time > EndTime ->
                              Result = 1;
                              holds(Time, Vals, R1),
                              (
                                R1 = 1 ->
                                holdsBefore(EndTime, Vals, T, R2),
                                Result is R1 * R2
                                ;
                                Result  = R1
                              )
                            ).


%%%%% next %%%%%

next(F1, F2, Result, TLIN, TLOUT, Spacer):-
                  printChecking(next(F1, F2), "?", Spacer),
                  string_concat(Spacer, "   ", NewSpacer1),
                  printChecking("Finding Pre-States", "?", NewSpacer1),

                  %find all where F1 is true in TLIN, store in TLOUT1
                  string_concat(NewSpacer1, "   ", NewSpacer2),
                  findAllWhereTrue(F1, TLIN, TLOUT1, NewSpacer2),

                  %output
                  printChecking("Finding Pre-States", TLOUT1, NewSpacer1),
                  (
                    TLOUT1 = [] ->
                    Result = 1, TLOUT = TLOUT1;
                    (
                      %find all next timepoints of each in TLOUT store in TLNEXT
                      addOneToTimePoints(TLOUT1, TLNEXT),

                      %output
                      printChecking("Checking Post-States", TLNEXT, NewSpacer1),

                      %check F2 holds in each TLNEXT
                      checkHoldsAtEach(F2, TLNEXT, Result, NewSpacer2),
                      (
                        Result = 1 ->
                        TLOUT = TLOUT1;
                        TLOUT = []
                      ),

                      %output
                      printChecking("Checking Post-states", Result, NewSpacer1)
                    )
                ),
                printChecking(next(F1, F2), Result, Spacer).


%%%%%%% always %%%%%%%

always(F1, F2, Result, TLIN, TLOUT, Spacer):-
  printChecking(always(F1, F2), "?", Spacer),
  string_concat(Spacer, "   ", NewSpacer1),
  printChecking("Finding Pre-States", "?", NewSpacer1),

  %find all where F1 is true in TLIN, store in TLOUT1
  string_concat(NewSpacer1, "   ", NewSpacer2),
  findAllWhereTrue(F1, TLIN, TLOUT1, NewSpacer2),

  %output
  printChecking("Finding Pre-States", TLOUT1, NewSpacer1),
  (
    TLOUT1 = [] ->
    Result = 1, TLOUT = TLOUT1;
    (
      %output
      printChecking("Checking Post-States", "?", NewSpacer1),

      %check F2 holds from each time in TLOUT1 to end time
      checkHoldsTillEnd(F2, TLOUT1, Result, NewSpacer2),
      (
        Result = 1 ->
        TLOUT = TLOUT1;
        TLOUT = []
      ),

      %output
      printChecking("Checking Post-states", Result, NewSpacer1)
    )
),
printChecking(always(F1, F2), Result, Spacer).


%%%%%%% eventually %%%%%%%

eventually(F1, F2, Result, TLIN, TLOUT, Spacer):-
  printChecking(eventually(F1, F2), "?", Spacer),
  string_concat(Spacer, "   ", NewSpacer1),
  printChecking("Finding Pre-States", "?", NewSpacer1),

  %find all where F1 is true in TLIN, store in TLOUT1
  string_concat(NewSpacer1, "   ", NewSpacer2),
  findAllWhereTrue(F1, TLIN, TLOUT1, NewSpacer2),

  %output
  printChecking("Finding Pre-States", TLOUT1, NewSpacer1),
  (
    TLOUT1 = [] ->
    Result = 1, TLOUT = TLOUT1;
    (
      %output
      printChecking("Checking Post-States", "?", NewSpacer1),

      %check F2 holds eventually in the future from each TLOUT1
      checkHoldsSomeTime(F2, TLOUT1, Result, NewSpacer2),
      (
        Result = 1 ->
        TLOUT = TLOUT1;
        TLOUT = []
      ),

      %output
      printChecking("Checking Post-states", Result, NewSpacer1)
    )
),
printChecking(eventually(F1, F2), Result, Spacer).


findAllWhereTrue(next(_, _), [], _,  _) :- !.
findAllWhereTrue(next(F1, F2), TLIN, TLOUT, Spacer) :-
                                    next(F1, F2, _, TLIN, TLOUT, Spacer).

findAllWhereTrue(always(_, _), [], _,  _) :- !.
findAllWhereTrue(always(F1, F2), TLIN, TLOUT, Spacer) :-
                                    always(F1, F2, _, TLIN, TLOUT, Spacer).

findAllWhereTrue(eventually(_, _), [], _,  _) :- !.
findAllWhereTrue(eventually(F1, F2), TLIN, TLOUT, Spacer) :-
                                    eventually(F1, F2, _, TLIN, TLOUT, Spacer).


% NOTE: has to be last, so it unifies with [val1,val2]
findAllWhereTrue(_, [], [], _) :- !.
findAllWhereTrue(F, [H|TL], TL1, Spacer) :- !, findAllWhereTrue(F, TL, TL2, Spacer),
                                            (holds(H, F, R1, Spacer),
                                            R1 = 1 ->
                                             TL1 = [H | TL2];
                                             TL1 = TL2
                                           ).

checkHoldsAtEach(next(_, _), [], 1, _) :- !.
checkHoldsAtEach(next(F1, F2), TLIN, Result, Spacer) :-
                        next(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsAtEach(always(_, _), [], 1, _) :- !.
checkHoldsAtEach(always(F1, F2), TLIN, Result, Spacer) :-
                        always(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsAtEach(eventually(_, _), [], 1, _) :- !.
checkHoldsAtEach(eventually(F1, F2), TLIN, Result, Spacer) :-
                        eventually(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsAtEach(_, [], 1, _) :- !.
checkHoldsAtEach(F, [H|TL], Result, Spacer) :- holds(H, F, R1, Spacer),
                                        ( R1 = 1 ->
                                          checkHoldsAtEach(F, TL, R2, Spacer),
                                          Result is R1 * R2;
                                          Result = 0
                                        ).

checkHoldsTillEnd(_, [], 1, _) :- !.
checkHoldsTillEnd(F2, [H|T], Result, Spacer) :-
                                  %trace,
                                  findall(Time, (log(Time, _), Time >= H), TLToEnd ),
                                  checkHoldsAtEach(F2, TLToEnd, R1, Spacer),
                                  (
                                    R1 = 1 ->
                                      checkHoldsTillEnd(F2, T, R2, Spacer),
                                      Result = R2;
                                      Result = 0
                                  ).


checkHoldsSomeTime(next(_,_), [], 1, _) :-!.
checkHoldsSomeTime(next(F1,F2), TLIN, Result, Spacer) :-
                                next(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsSomeTime(always(_,_), [], 1, _) :-!.
checkHoldsSomeTime(always(F1,F2), TLIN, Result, Spacer) :-
                                always(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsSomeTime(eventually(_,_), [], 1, _) :-!.
checkHoldsSomeTime(eventually(F1,F2), TLIN, Result, Spacer) :-
                                eventually(F1, F2, Result, TLIN, TLOUT, Spacer).

checkHoldsSomeTime(_, [], 1, _) :-!.
checkHoldsSomeTime(F2, [H|T], Result, Spacer) :-
                                findall(Time, (log(Time, _), Time >= H), TLToEnd ),
                                checkHoldsSomeTime2(F2, TLToEnd, R1, Spacer),
                                (
                                  R1 = 1 ->
                                    checkHoldsSomeTime(F2, T, R2, Spacer),
                                    Result = R2;
                                    Result = 0
                                ).




checkHoldsSomeTime2(_, [], 0, _) :- !.
checkHoldsSomeTime2(F, [H|TL], Result, Spacer) :- holds(H, F, R1, Spacer),
                                        ( R1 = 1 ->
                                          Result = 1;
                                          checkHoldsSomeTime2(F, TL, R2, Spacer),
                                          Result is R2
                                        ).
