/*
* Mission Requirements for Testing
* Syntax: req(name, priorityLevel, temporalOperator)
* Actual Data to be uploaded user
*/
req(test16, critical,
  eventually(
    val(loc,5,5),
    val(d, on )
  )
).

/* Specific Temporal Ops */

/*
req(test1,critical,
    holds(0, val(fuel, 50))
).


req(test2,critical,
    [
     holds(0, val(fuel, 50)),
     holds(1, val(fuel, 48))
    ]
).

req(test3,critical,
    [
     holds(0, val(fuel, 50)),
     holds(1, val(fuel, 49))
    ]
).

req(test4,critical,
  or(
    holds(0, val(fuel, 50)),
    holds(1, val(fuel, 49))
  )
).

req(test5,critical,
    [
     holds(0, val(fuel, 50)),
     or(
        holds(1, val(fuel, 48)),
        holds(1, val(fuel, 49))
     )
    ]
).

req(test6, critical,
   or(
      holds(0, val(fuel, 50)),
      [
        holds(0, val(alt, 50)),
        holds(0, val(det, false))
      ]
    )
).

req(test7, critical,
   or(
      holds(0, val(fuel, 5)),
      [
        holds(0, val(alt, 50)),
        holds(0, val(det, false))
      ]
    )
).

req(test8, critical,
   or(
      holds(0, val(fuel, 5)),
      [
        holds(0, val(alt, 40)),
        holds(0, val(det, false))
      ]
    )
).

req(test9, critical,
  holds(1, range(alt, 30, 50))
).

req(test10, critical,
  holds(16, range(loc, 0, 0, 6))
).

*/

/* General Temporal Ops */


/*
req(test11, critical,
  next(val(det, true), val(comms, off))
).

req(test12, critical,
  next([range(loc,0,0,2), val(msgRcvd,true)], val(msgAck, true))
).

req(test13, critical,
  next(val(det, true), [val(comms, off), val(jammer, on)])
).

req(test134, critical,
  next(val(det, true), val(det, false))
).

req(test15, critical,
  next(val(det, true), next(val(det, true), val(det, false)))
).

//always

req(test16, critical,
  always(true, range(fuel, 29, 50 ))
).

req(test17, critical,
  always(val(loc,5,5), range(fuel, 20, 50 ))
).

req(test18, critical,
  always(val(det,true), range(fuel, 20, 50 ))
).



req(test20, critical,
  next(val(det,true), always(true, val(det, false)))
).


req(test21,critical,
 always(
  val(loc,2,5),
  next(
   val(msgRcvd,true),
   val(msgAck,true)
  )
 )
).




req(test25, critical,
  eventually(val(loc,0,0), val(loc,0,0))
).


req(test25, critical,
  eventually(val(det,true), val(det, false))
).

*/
