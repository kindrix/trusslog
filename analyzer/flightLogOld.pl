/*
* Flight Log Data for Testing
* Actual data to be uploaded by user with web interface
*/

  log(1, [
    val(alt, 50),
    val(loc, 3, 4),
    val(fuel, 45)
  ]).

log(2, [
  val(alt, 50),
  val(loc, 6, 4),
  val(fuel, 40)

]).

log(3, [
  val(alt, 50),
  val(loc, 6, 4),
  val(fuel, 30)
]).

log(4, [
  val(alt, 10),
  val(loc, 6, 4),
  val(fuel, 40)
]).

log(5, [
  val(alt, 50),
  val(loc, 6, 4),
  val(fuel, 40)
]).
